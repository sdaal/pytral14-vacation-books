from django.shortcuts import render, redirect
from django.http import HttpRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .models import Book


def get_book_list(request: HttpRequest) -> HttpResponse:
    books = Book.objects.all() # select * from books
    return render(request, "book_list.html", context={
        "books": books
    })


@csrf_exempt
def add_book(request: HttpRequest) -> HttpResponse:
    print("METHOD:", request.method)
    print("Body:", request.body)
    print("POST:", request.POST)
    if request.method == "POST":
        title = request.POST["title"]
        author = request.POST["author"]
        book = Book(title=title, author=author)
        book.save()
        return redirect(book)
    return render(request, "book_form.html", context={})


def get_book_details(request: HttpRequest, pk: int) -> HttpResponse:
    book = Book.objects.get(pk=pk)
    return render(request, "book_details.html", context={
        "book": book,
    })


@csrf_exempt
def update_book(request: HttpRequest, pk: int) -> HttpResponse:
    book = Book.objects.get(pk=pk)
    if request.method == "POST":
        title = request.POST["title"]
        author = request.POST["author"]
        book.title = title
        book.author = author
        book.save()
        return redirect(book)
    return render(request, "book_form.html", context={
        "book": book,
    })


@csrf_exempt
def remove_book(request: HttpRequest, pk: int) -> HttpResponse:
    book = Book.objects.get(pk=pk)
    if request.method == 'POST':
        book.delete()
        return redirect("book-list")
    return render(request, "book_confirm_delete.html", context={
        "book": book,
    })


@csrf_exempt
@require_POST
def mark_as_read(request: HttpRequest, pk: int) -> HttpResponse:
    book = Book.objects.get(pk=pk)
    book.has_read = True
    book.save()
    return redirect(book)
