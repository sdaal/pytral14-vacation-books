from django.db import models
from django.urls import reverse


class Book(models.Model):   # books_book
    title = models.CharField(max_length=256, null=False)
    author = models.CharField(max_length=256)
    has_read = models.BooleanField(default=False)

    class Meta:
        db_table = "books"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("book-details", kwargs={"pk": self.pk})
