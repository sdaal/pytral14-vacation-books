from django.urls import path

from . import views

urlpatterns = [
    path("", views.get_book_list, name="book-list"),
    path("add/", views.add_book, name="book-add"),
    path("<int:pk>/", views.get_book_details, name="book-details"),
    path("<int:pk>/edit/", views.update_book, name="book-update"),
    path("<int:pk>/delete/", views.remove_book, name="book-delete"),
    path("<int:pk>/read/", views.mark_as_read, name="book-read"),
]
