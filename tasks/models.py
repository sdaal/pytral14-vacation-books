from django.db import models


class Task(models.Model):   # tasks_task
    task = models.CharField(max_length=256, null=False)
    is_done = models.BooleanField(default=False)

    class Meta:
        db_table = "tasks"

    def __str__(self):
        return self.task
