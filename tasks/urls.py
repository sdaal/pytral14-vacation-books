from django.urls import path

from . import views

urlpatterns = [
    path("list/", views.get_task_list, name="task-list"),
]
