from django.shortcuts import render

from .models import Task


def get_task_list(request):
    tasks = Task.objects.all()
    return render(request, "task_list.html", context={
        "tasks": tasks
    })
